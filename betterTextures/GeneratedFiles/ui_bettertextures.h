/********************************************************************************
** Form generated from reading UI file 'bettertextures.ui'
**
** Created by: Qt User Interface Compiler version 5.1.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BETTERTEXTURES_H
#define UI_BETTERTEXTURES_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_betterTexturesClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *betterTexturesClass)
    {
        if (betterTexturesClass->objectName().isEmpty())
            betterTexturesClass->setObjectName(QStringLiteral("betterTexturesClass"));
        betterTexturesClass->resize(600, 400);
        menuBar = new QMenuBar(betterTexturesClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        betterTexturesClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(betterTexturesClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        betterTexturesClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(betterTexturesClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        betterTexturesClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(betterTexturesClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        betterTexturesClass->setStatusBar(statusBar);

        retranslateUi(betterTexturesClass);

        QMetaObject::connectSlotsByName(betterTexturesClass);
    } // setupUi

    void retranslateUi(QMainWindow *betterTexturesClass)
    {
        betterTexturesClass->setWindowTitle(QApplication::translate("betterTexturesClass", "betterTextures", 0));
    } // retranslateUi

};

namespace Ui {
    class betterTexturesClass: public Ui_betterTexturesClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BETTERTEXTURES_H
