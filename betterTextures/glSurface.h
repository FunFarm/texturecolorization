#pragma once
#ifndef _glSurface
#define _glSurface

#include <Windows.h>
#include <Mmsystem.h>
#include <GL/glew.h>
#include <GL/wglew.h>
#include <GL/gl.h>
//
#include <vector>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <iostream>
#include <thread>
#include "spinningBarrier.h"
//
#include <QtGui\qimage.h>

using namespace std;

class glSurface 
{
public:
	glSurface(vector<string>& phaseShiftsIn, int monitorIn = 2 );
	virtual ~glSurface(void);
	virtual void setFrame(int framenum);
	virtual void loadFrames(vector<string>& images);
private:
	vector<QImage> frames;
	void close();
	int Init();
	void createWindow();
	bool hasValidGlContext;
	DWORD m_idThread;
	int g_windowWidth,g_windowHeight;
	void InitGL();
	void renderFrame();
	static	LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	WNDCLASSEX wcl;
	HWND      g_hWnd;
	HINSTANCE g_hInstance;
	HDC       g_hDC;
	HGLRC     g_hRC;
	HWND hWnd;
	MONITORINFO monitorInfo;
	//For openGL
	int frame;
	float* vertices;
	float* texCoordinates;
	GLuint textures[4];
	GLuint lut[1];
	GLuint sampler[1];
	GLuint programHandle;
	GLuint vaoHandle;
	GLuint* vboHandles;
	static void fillRandomNoise(GLbyte* noise,int n);
	void initShaders();
	int windowLoop();
	int glLoop(vector<string>& images);
	int monitor; 
	//For synchronization
	spinning_barrier* sb;
	bool ping, pong;
	mutex* m;
	condition_variable* cv;
	bool done;
	thread* windowThread;
	thread* openglThread;
};

#endif