﻿#include "glSurface.h"

//Globals
#define glE() _glE(__FILE__,__LINE__)
void _glE(const char *file, int line)
{
#ifndef brave
	GLenum err=glGetError();
	if (!err)
	{
		return;
	}
	const char*  errString =(const char*)gluErrorString(err);
	cout <<"Glu Error:"<<file <<":"<<line <<": " << errString << endl;
	exit(20);
#endif
}

#define shaderCheck(sh) _shaderCheck(sh,__FILE__,__LINE__)
void _shaderCheck(GLint sh,const char *file, int line)
{
	GLint status;
	glGetShaderiv( sh, GL_COMPILE_STATUS, &status );
	if (GL_FALSE == status)
	{
		GLint logLen;
		glGetShaderiv( sh, GL_INFO_LOG_LENGTH, &logLen );
		if( logLen > 0 )
		{
			char * log = (char *)malloc(logLen);
			GLsizei written=0;
			glGetShaderInfoLog(sh, logLen, &written, log);
			cout <<"SHDR Error:"<<file <<":"<<line <<": " <<log << endl;
			free(log);
		}
		exit(20);
	}
}

#define programCheck(ph) _programCheck(ph,__FILE__,__LINE__)
void _programCheck(GLint ph,const char *file, int line)
{
	GLint status;
	glGetProgramiv( ph, GL_LINK_STATUS, &status );
	if (GL_FALSE == status)
	{
		GLint logLen;
		glGetProgramiv( ph, GL_INFO_LOG_LENGTH, &logLen );
		if( logLen > 0 )
		{
			char * log = (char *)malloc(logLen);
			GLsizei written=0;
			glGetProgramInfoLog(ph, logLen, &written, log);
			cout <<"PRGM Error:"<<file <<":"<<line <<": " <<log << endl;
			free(log);
		}
		exit(20);
	}
}
glSurface* that = 0;
int oneWeWant = 0;

//Functions
glSurface::glSurface(vector<string>& phaseShiftsIn, int monitorIn) : monitor(monitorIn)
{
	oneWeWant=monitorIn;
	that = this;
	cv = new condition_variable();
	hasValidGlContext= false;
	m = new mutex();
	//Window Thread (nothing interesting here)
	sb = new spinning_barrier(2);
	windowThread = new thread(&glSurface::Init,this);	
	sb->wait();
	delete sb;
	//Could happen from windowThread in a updated version
	sb = new spinning_barrier(2);
	openglThread = new thread(&glSurface::glLoop,this,phaseShiftsIn);	
	sb->wait();
	delete sb;
}

void glSurface::loadFrames(vector<string>& images)
{
	frames.clear();
	glE();
	glActiveTexture(GL_TEXTURE0);
	glDeleteTextures(4,textures);
	glGenTextures(4,textures);
	glE();
	for (int i = 0 ; i < 4; i++)
	{
		int n = 1*g_windowHeight*g_windowWidth;
		//
		auto a = QImage(images[i].c_str()).mirrored(false,true).convertToFormat(QImage::Format_Indexed8).scaled(g_windowWidth,g_windowWidth);
		frames.push_back(a);
		//
		glBindTexture(GL_TEXTURE_2D,textures[i]);
		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_R8,
			g_windowWidth,g_windowHeight,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			frames[i].bits());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glBindTexture(GL_TEXTURE_2D,0);
	}
	glE();
	//
	glActiveTexture(GL_TEXTURE1);
	glGenTextures(1,lut);
	glBindTexture(GL_TEXTURE_1D,lut[0]);
	unsigned char* linear = new unsigned char[256];//should be a 1->1 mapping
	for (int i = 0 ;i < 256;i++)
	{
		linear[i] = i;
	}
	glTexImage1D(GL_TEXTURE_1D, 0, GL_R8, 256, 0,GL_RED, GL_UNSIGNED_BYTE, linear);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAX_LEVEL, 0);
	glBindTexture(GL_TEXTURE_1D, 0);
	delete linear;
	//
	glGenSamplers(1,sampler);
	glSamplerParameteri(sampler[0], GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glSamplerParameteri(sampler[0], GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glSamplerParameteri(sampler[0], GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);	
	//
	glE();
}

void glSurface::initShaders()
{
	//Reserve the textures and make the shaders
	const char *vsrc1 =
		"attribute vec2 vecPosIn;\n"
		"attribute vec4 texPosIn;\n"
		"varying vec4 texPosOut;\n"
		"void main()\n"
		"{\n"
		"gl_Position = vec4(vecPosIn, 0.0, 1.0);\n"
		"texPosOut = texPosIn;\n"
		"}\n";
	GLint vsH = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vsH,1,&vsrc1,NULL);
	glCompileShader(vsH);
	shaderCheck(vsH);
	glE();
	const char *fsrc1 =
		"uniform sampler2D tex0;\n"
		"uniform sampler1D tex1;\n"
		"varying vec4 texPosOut;\n"
		"void main(void)\n"
		"{\n"
		//"    gl_FragColor = texture2D(tex0, texPosOut.st);\n"
		"    float red = texture2D(tex0, texPosOut.st).r;\n"
		"    gl_FragColor = texture1D(tex1, red);\n"
		"}\n";
	GLint fsH = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fsH,1,&fsrc1,NULL);
	glCompileShader(fsH);
	shaderCheck(fsH);
	glE();
	//Bind
	programHandle = glCreateProgram();
	glBindAttribLocation(programHandle,0,"vecPosIn");
	glBindAttribLocation(programHandle,1,"texPosIn");
	glBindAttribLocation(programHandle,2,"texPosOut");
	glE();
	//Link
	glAttachShader( programHandle, vsH );
	glAttachShader( programHandle, fsH );
	glLinkProgram( programHandle );
	programCheck(programHandle);
	glE();
	//Set render() invariants
	glUseProgram( programHandle );
	GLuint idk = glGetUniformLocation(programHandle, "tex1");
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_1D,lut[0]);
	glUniform1i(idk,0);
	glBindTexture(GL_TEXTURE_1D,0);
	glUseProgram( 0 );
	glE();
	//Pack static data into a VAO
	float vecData[] = {
		-1, 1,
		-1,-1,
		1, 1,
		-1,-1,
		1,-1,
		1, 1
	};
	float texData[] = { 
		0,1,
		0,0,
		1,1,
		0,0,
		1,0,
		1,1
	};
	vboHandles=(GLuint*)malloc(sizeof(int)*2);
	glGenBuffers(2,vboHandles);
	GLuint vecBuf=vboHandles[0];
	GLuint texBuf=vboHandles[1];
	glBindBuffer(GL_ARRAY_BUFFER, vecBuf);
	glBufferData(GL_ARRAY_BUFFER, 8*2 * sizeof(GLfloat), vecData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, texBuf);
	glBufferData(GL_ARRAY_BUFFER, 8*2 * sizeof(GLfloat), texData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glE();
	glGenVertexArrays( 1, &vaoHandle );
	glBindVertexArray(vaoHandle);
	glEnableVertexAttribArray(0);  // Vertex position
	glEnableVertexAttribArray(1);  // Vertex color
	glBindBuffer(GL_ARRAY_BUFFER, vecBuf);
	glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );
	glBindBuffer(GL_ARRAY_BUFFER, texBuf);
	glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL ); //2 attribute per vertex
	glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);
	glE();

}

BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
{

	static int num = 0;
	if ((num++)==oneWeWant)
	{
		auto monitorInfo = (MONITORINFO*) dwData;
		monitorInfo->cbSize = sizeof(MONITORINFO);
		GetMonitorInfo(hMonitor,monitorInfo);
	}
	return TRUE;
}
void glSurface::createWindow()
{
	//Application Window
	auto hInstance = GetModuleHandle(NULL);
	wcl.cbSize = sizeof(wcl);
	wcl.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	wcl.lpfnWndProc = WindowProc;
	wcl.cbClsExtra = 0;
	wcl.cbWndExtra = 0;
	wcl.hInstance = g_hInstance = hInstance;
	wcl.hIcon = LoadIcon(0, IDI_APPLICATION);
	wcl.hCursor = LoadCursor(0, IDC_ARROW);
	wcl.hbrBackground = 0;
	wcl.lpszMenuName = 0;
	wcl.lpszClassName = L"foo_window";
	wcl.hIconSm = 0;
	RegisterClassEx(&wcl);
	DWORD wndExStyle = 0;
	DWORD wndStyle =  WS_POPUP | WS_SYSMENU;
	g_hWnd = CreateWindowEx(wndExStyle,wcl.lpszClassName, L"foo", 
		wndStyle, 0, 0, 0, 0, 0, 0, wcl.hInstance, 0);
	assert(g_hWnd);
	//Move to monitor
	EnumDisplayMonitors(NULL, NULL, MonitorEnumProc,(LPARAM)&monitorInfo);
	RECT rc = monitorInfo.rcMonitor;
	MoveWindow(g_hWnd, rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top, TRUE);
	GetClientRect(g_hWnd, &rc);
	g_windowWidth = rc.right - rc.left;
	g_windowHeight = rc.bottom - rc.top;
}


void glSurface::InitGL()
{
	//Makes context "the hard way"
	g_hDC = GetDC(g_hWnd);
	int pf = 0;
	//GDI Part (write once, forget)
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 16;
	pfd.cDepthBits = 8;
	pfd.iLayerType = PFD_MAIN_PLANE;
	OSVERSIONINFO osvi = {0};
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx(&osvi);
	pf = ChoosePixelFormat(g_hDC, &pfd);
	SetPixelFormat(g_hDC, pf, &pfd);
	if (osvi.dwMajorVersion > 6 || (osvi.dwMajorVersion == 6 && osvi.dwMinorVersion >= 0))
	{
		pfd.dwFlags |= PFD_SUPPORT_COMPOSITION;
	}
	//OpenGL Part
	//Bootstrap glew
	HGLRC tempContext = wglCreateContext(g_hDC);
	wglMakeCurrent(g_hDC, tempContext);
	auto foo = glewInit();
	assert(foo == GLEW_OK);
	int attribList[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 1,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
		0, 0
	};
	//Make real context
	g_hRC = wglCreateContextAttribsARB(g_hDC,0, attribList);
	wglMakeCurrent(NULL,NULL);
	wglDeleteContext(tempContext);
	wglMakeCurrent(g_hDC, g_hRC);
	auto fun = glGetString(GL_VERSION);
	cout << "OpenGL version is " << fun << endl;
	SetWindowText(g_hWnd, L"foo");
}

void glSurface::renderFrame()
{
	//SetupWindow
#if 1
	static int tic = -1;
	tic = ++tic %2;
	glClearColor(tic, tic, tic, 0.0f);
#endif
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glClear(GL_COLOR_BUFFER_BIT);
	glE();
	//Bind
	glUseProgram(programHandle);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,textures[frame]);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_1D,lut[0]);
	GLuint idk = glGetUniformLocation(programHandle, "tex1");
	glUniform1i(idk,0);
	glBindSampler(0,sampler[0]);glE();
	glE();
	//Draw
	glBindVertexArray(vaoHandle);
	glDrawArrays(GL_TRIANGLES,0,8);
	glBindVertexArray(0);
	glE();
	//Unbind
	glActiveTexture(GL_TEXTURE1);
	glBindSampler(0,0);
	glBindTexture(GL_TEXTURE_1D,0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,0);
	glUseProgram(0);
	glE();
	//DumpFrame
	glFlush();
	glFinish();
	glE();
}

void glSurface::setFrame(int framenum)
{

	frame = framenum;
	//Sets the frame and locks until next operation is complete
	{
		std::lock_guard<std::mutex> lk(m[0]);
		ping = true;
	}
 	cv->notify_one();
	{
		std::unique_lock<std::mutex> lk(m[0]);
		cv->wait(lk, [&]{return pong;});
	}
	pong = false;
}



int glSurface::glLoop(vector<string>& images)
{
	//Assert intial state
	const int p=1;
	timeBeginPeriod(p);
	frame = 0;
	done = false; 
	//Make OpenGL Window
	InitGL();
	wglSwapIntervalEXT(1);
	loadFrames(images);
	initShaders();
	ShowWindow(g_hWnd,SW_MAXIMIZE);
	//Often idles
	sb->wait();
	while (!done)
	{
		{
			std::unique_lock<std::mutex> lk(m[0]);
			cv->wait(lk, [&]{return ping;});
		}
		ping = false;
		//Display Frame
		renderFrame();
		SwapBuffers(g_hDC);
		{
			std::lock_guard<std::mutex> lk(m[0]);
			pong = true;
		}
		cv->notify_one();
	}
	//Clean up
	wglDeleteContext(g_hRC);
	g_hRC = 0;
	ReleaseDC(g_hWnd, g_hDC);
	timeEndPeriod(p);
	return 0;
}


void glSurface::close()
{
	done = true;
	setFrame(1);//dirty hack for tear down
}

glSurface::~glSurface(void)
{//check for memory leaks later
	close();
	openglThread->join();
	delete openglThread;
	PostThreadMessage(m_idThread,WM_QUIT,0,0);//PostQuitMessage(0);
	windowThread->join();
	delete windowThread;
}

int glSurface::Init()
{
	createWindow();
	m_idThread = GetCurrentThreadId();
	sb->wait();
	windowLoop();
	g_hDC = 0;
	UnregisterClass(wcl.lpszClassName,wcl.hInstance);
	return 0;
}

int glSurface::windowLoop()
{
	MSG msg;
	// loop until WM_QUIT(0) received
	while(GetMessage(&msg, NULL, 0, 0) > 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return (int)msg.wParam;
}

LRESULT CALLBACK glSurface::WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CHAR:
		switch (static_cast<int>(wParam))
		{
		case VK_ESCAPE:
			PostMessage(hWnd, WM_CLOSE, 0, 0);
			break;

		default:
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_CREATE:
		break;

	default:
		break;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}